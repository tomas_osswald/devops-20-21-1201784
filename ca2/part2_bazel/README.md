# Class Assignment 2 Report

We arrive at the third and final part of our tutorial for class assignment two. Those of you who made it this far undoubtedly possess inhuman courage and would laugh in the face of death. What expects you, however, is not death, but something much worse. Introducing...

## Alternative to Gradle - BAZEL
========================

### What is Bazel?

Bazel is a build tool developed by Google, based on a build tool called Blaze they use internally. Build rules and macros used to build applications are created in Starlark, a dialect of python. Built-in rules for various programming languages exist, including Java.


### 0. Setup

Let's create a new Bazel project based on the Springboot tutorial from ca1.

*Make sure the directories where your bazel project is located do not contain spaces*

- Go ahead and copy the "src" folder from that project into the root folder of your bazel project. Next create a *BUILD* file and a *WORKSPACE* file.

1. Build

- Your BUILD files contain instructions for Bazel on how to build your project. The most important type of instructions is the *build rule*. These are called *target* and point to specific source files and dependencies, or even other targets. An example can be found [here](https://github.com/bazelbuild/examples/blob/master/java-tutorial/BUILD).

- Let's copy the BUILD file from the example and change the java_binary rule:

```
java_binary(
    name = "runApp",
    srcs = glob(["src/main/java/com/greglturnquist/payroll/*.java"]),
    main_class = "ReactAndSpringDataRestApplication.java",
)
```

Java_binary is a built-in Bazel rule that builds a .jar file and a wrapper shell script. The attribute name is mandatory and will be the name of the built files. The *srcs* attribute specifies source files and an additional *main_class* attribute may specify a class containing the main method.

- Try to build your project using:

```
bazel build //:runApp
```

// is the location of the build file in our project and runApp is the name we chose for our target.


### 1. Oh no.

It seems our build did not complete succesfully. Further inspection of the error messages tells us that SpringBoot is not being recognized. Well if you did your research beforehand, you might have found out that there is no native support for SpringBoot using Bazel.

Don't fret, not all is lost! It is time to dive into the internet in search of answers.


### 2. We're back, baby!

Well that was fast. After some google searches, we find a github project containing [the Spring Boot rule for the Bazel build system](https://github.com/salesforce/rules_spring). Sounds promising!

Let's get to work. If we needed to build an executable jar file for our project then we would need to follow the steps on this project. However for our tutorial we expect all environments that will launch this application to have Bazel available. As such, we can go into the [rules_jvm_external Spring Boot example](https://github.com/bazelbuild/rules_jvm_external/tree/master/examples/spring_boot).

Now this seems like a nice simple project we can use as a starting point. Go ahead and clone the repository using the following link:

https://github.com/bazelbuild/rules_jvm_external.git

And copy rules_jvm_external/examples/spring_boot into your project directory.

Notice how the *BUILD* file in the your project's main directory is actually empty. If you navigate to the directories containing your Java classes, you'll find other *BUILD* files with targets to run.

Notice also your *WORKSPACE* file. There should be rules pointing to Spring Boot dependencies. Seems good!

- Following the instructions on the readme.md file you cloned just now, you can build the Sprin Boot application using:

```
bazel build //src/main/java/hello:app
```

- Try running the application using:

```
bazel run //src/main/java/hello:app
```

- You can run tests using:

```
bazel test //src/test/java/hello:HelloControllerTest
bazel test //src/test/java/hello:HelloControllerIntegrationTest
```


### 3. Pain.

We have hit a wall again. It seems we have a situation with our *WORKSPACE* files.

But this is not a tutorial for quitters. Seems like this should have an easy fix.

Instead of just copying the rules_jvm_external/examples/spring_boot folder into our directory, let's copy the whole repository.

### 4. Celebrate Good Times!

They said it couldn't be done, but we did it! The nuclear option was maybe unnecessary, but we take these wins.

Go ahead and try running those commands, you should only see glorious success messages popping everywhere.


### 5. Implementing our project

After these few difficult steps, let's get on with our mission. This example project works fine, but we would like to implement the project we worked on using gradle.

Go ahead and follow the part2 readme.md Step 1 instructions on copying and deleting files for the new project, remembering to keep those nice *BUILD* files in the same directory as your Java classes. Now we need to edit the *BUILD* files to match our new java files.

- Change the java_binary target in your main classes folder to the following:

```
java_binary(
    name = "app",
    main_class = "payroll.ReactAndSpringDataRestApplication",
    runtime_deps = [
        ":lib",
    ],
)
```

- Change the java_binary target in your test classes folder to the following:

```
java_test(
    name = "EmployeeTest",
    srcs = ["HelloControllerTest.java"],
    test_class = "hello.HelloControllerTest",
    deps = [
        "//src/main/java/hello:lib",
        "@maven//:org_hamcrest_hamcrest_library",
        "@maven//:org_springframework_boot_spring_boot_test",
        "@maven//:org_springframework_boot_spring_boot_test_autoconfigure",
        "@maven//:org_springframework_spring_beans",
        "@maven//:org_springframework_spring_test",
        "@maven//:org_springframework_spring_web",
    ],
)
```

Try building and running your project and tests using the following commands:

```
bazel build //src/main/java/com/greglturnquist/payroll:app

bazel run //src/main/java/com/greglturnquist/payroll:app

bazel test //src/test/java/com/greglturnquist/payroll:EmployeeTest
```

Your build has probably failed, but the solution seems pretty easy this time, as Bazel itself suggests adding a dependency to your project. Go to your *BUILD* file in our main classes directory and add the following dependency to the java_library target:

```
"@maven//:org_springframework_spring_beans",
```

Try running it again.


### 6. Sadness

Well that didn't work.

Some dependencies seem to be missing.


### 7. A New Hope

It seems our problem here is that our project does not have dependencies for Thymeleaf, JPA or H2.

In our part2 project we can identify these dependencies in the *build.gradle* file. If we look at our *WORKSPACE* file we can see it has some similar dependencies for Spring Boot, but not the ones we need. It seems Bazel only has dependencies that end on .RELEASE. 

With some googling, we can find [search.maven.org](search.maven.org). This is a maven central repository search tool where we can find all maven repositories. Searching for the dependencies in our Gradle project we can look for these versions.

- Let's try adding the following dependencies to our *WORKSPACE* file:

```
        "org.springframework.boot:spring-boot-starter-data-jpa:2.3.1.RELEASE",
        "org.springframework.boot:spring-boot-starter-data-rest:2.3.10.RELEASE",
        "org.springframework.boot:spring-boot-starter-thymeleaf:2.3.10.RELEASE",
        "org.springframework.boot:spring-boot-starter-test:2.3.10.RELEASE",
        "com.h2database:h2:1.4.200",
```

- And don't forget adding those dependencies on the java_library in your *BUILD* file:

```
        "@maven//:org_springframework_boot_starter_data_jpa",
        "@maven//:org_springframework_boot_starter_data_rest",
        "@maven//:org_springframework_boot_starter_thymeleaf",
        "@maven//:org_springframework_boot_starter_test",
        "@maven//:com_h2database_h2",
```

You'll notice that there is no h2 dependency that ends in .RELEASE. However it's not really worth the look, because your project still won't build.


### 8. Depression

It seems these lines did absolutely nothing.

The *BUILD* file does not recognize these dependencies.

And it's getting late. We should move on to better things such as...


### 9. Creating a copy and delete rules

Let's get this over with already.

For these rules we can use genrule(). This rule generates files from a shell command.

In your *BUILD* file add a genrule like this:

```
genrule(
    name = "copy",
    srcs = ["generated jar file location"],
    outs = ["dist"],
    cmd = "cp -r $(SRCS) $(OUTS)"
)
```

*srcs* and *outs* are the files we want to copy and their destination. *cmd* is the shell command we will use to to create this copy.

Next we create a delete task using:

```
genrule(
    name = "delete",
    srcs = ["src/resources/main/static/built/"],
    cmd = "rm -r $(SRCS)"
)
```

Now were it any other tutorial I would tell you to run these targets using:

```
bazel build //:copy
bazel build //:delete
```

However, our project does not work right now. So that's it.


### 10. Extending Bazel through rules/targets - Analysis

Moving past the obvious difficulties with this project regarding dependencies, we can look at how we can add other functions to Bazel through rules.

[Here](https://docs.bazel.build/versions/master/skylark/rules.html) we can find a more in-depth look at rules and how they can be created. Very much like Gradle, Bazel allows users to create many types of rules that can add conditions to the way the project is built. As we have seen with the example above, these rules can even be directed to use command line arguments, making pretty much any operation available in bash possible.

If you are not familiar with Python or Starlark, writing these rules will not be easy. Gradle's use of Groovy makes it much friendlier for programmers that work in Java.