# Class Assignment 3 Report


## PART2 - QEMU alternative
=================


### -1. Preconditions

To run vagrant using QEMU we need to install a few plugins beforehand. Libvirt is required, but it seems we can't find it for windows. We can't run the command *vagrant plugin install vagrant-libvirt* because of this.

QEMU seems to be suited for a Linux host machine.

Let's go back a bit.


## PART 2 - ~~~QEMU~~ Hyper-V alternative
=================

### -2. Introduction and analysis

Now this one is easier. Hyper-V is built into Windows Pro, Enterprise and Education as an optional feature. You can use it to emulate Windows and Linux guest machines.

Hyper-V runs on the host machine hardware, meaning it is always on if the host machine is on. VirtualBox on the other hand runs on the host operating system.

A few limitations that Hyper-V has is that it cannot configure new networks, meaning networking configurations in [Vagrantfile are ignored](https://www.vagrantup.com/docs/providers/hyperv/limitations).

It also requires the command line to be run as administrator.


### -1. Preconditions

To enable Hyper-V, open a PowerShell console as an administrator and use:

```
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All
```

Now we need a box that is compatible with Hyper-V, as the one we used is only compatible with VirtualBox. In the [Vagrant website](https://app.vagrantup.com/boxes/search?provider=hyperv) we can search for boxes compatible with different providers. For this tutorial we used [bento/ubuntu-16.04](https://app.vagrantup.com/bento/boxes/ubuntu-16.04) so we are using the same operating system as before.

- In your vagrant file, where appropriate, replace the existing line with this:

```
  config.vm.box = "bento/ubuntu-16.04"
```


### 0. Let's get it started

We need to specify the provider when using vagrant up now:

```
vagrant up --provider=hyperv
```

With this command both virtual machines should be up and running. However if you try to connect to the applications using your browser, you should find it's not possible. Remember, there is no static ip address now!

You can find out your virtual machine's ip address using the Hyper-V manager tool, under the networking tab. For out tutorial we had:

182.22.74.173 - db

172.22.71.67 - web

Log onto the virtual machines using *vagrant ssh [virtual machine name]* and edit the application.properties file so it has the correct ip address.

- Now you can use your browser to check the result with the new ip address:

```
http://172.22.71.67:8080/basic-0.0.1-SNAPSHOT/

http://172.22.71.67:8080/basic-0.0.1-SNAPSHOT/h2-console
```

Congratulations! You did it!