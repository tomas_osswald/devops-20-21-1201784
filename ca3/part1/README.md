# Class Assignment 3 Report

Virtualization time using Oracle VM Virtual Box. We are pretty late this time, but let's give it a go.

## PART1
=================

### 0. Setup

Download and install Oracle VM Virtual Box from this [link](https://www.virtualbox.org/wiki/Downloads).


### 1. Creating a new machine

- Once Virtual Box is installed, create a new Machine. Choose any name you like (we will be using "ubuntu-devops") and the folder where the machine files will be located. We want a Linux type machine, as it is one of the most used and versatile operating systems for our purposes.

- As for RAM memory, try to use at least 2048mb if your host machine allows it.

- Regarding Hard Disk drive, you should create a new Virtual Hard Disk, with a recommended size of 10gb. Use the other default options, such as VDI(VirtualBox Disk Image) and Dynamically allocated.


### 2. Prepare your Virtual Machine for startup

- Download the Ubuntu Installation/MinimalCD from [here](https://help.ubuntu.com/community/Installation/MinimalCD). For this tutorial we are using Ubuntu 18.04 "Bionic Beaver".

- In Virtual Box, choose your machine and click "Settings". Go to "Storage" and in your "Controller: IDE" slot choose the file you just downloaded. Remember to select Live CD/DVD to make sure the machine reads this file on startup.

**Network Adapters**

- Before starting your machine you need to setup two network adapters. The first one allows your virtual machine to access the web through your host machine. The second one will create a virtual local network.

- Start by going to File - Host Network Manager. If there is not a network already, you should create a new one.

- With your virtual machine selected, click "Settings" and go into the "Network" tab. There should be one adapter already enabled, attached to "NAT". Enable Adapter2 as a Host-Only Adapter and choose the network you created just above.


### 3. Installing Ubuntu

- Now everything is in place and you're ready to start. Select your virtual machine in Virtual Box and click "Start".

- Select install on the Ubuntu installer boot menu. Pick your preferred language and select your Keyboard. Pick your primary Network Interface, which should be your first choice if your Adapter 1 was attached to NAT.

- Go through the installation, if you are unsure on any options pick the default one.

- Once everything is done, switch back to Virtual Box, open your virtual machine Settings and remove the virtual disk you selected a few steps back.

You should now be greeted with a prompt for your username and password. Congratulations, you done did it.


### 4. Updating and installing basic tools and network

- Run a few commands to update your machine and install network tools:

```
sudo apt update
sudo apt install net-tools
sudo nano /etc/netplan/01-netcfg.yaml
```

This last one will open a file editor to edit your network configurations. It should look like this:

```
network:
  version: 2
  renderer: networkd
  ethernets:
    enp0s3:
      dhcp4: yes
    enp0s8:
      addresses:
        192.168.56.5/24
```

Finish your editing by entering the command:

```
sudo netplan apply
```

To make sure everything is working, you can check your networks using the command:

```
ifconfig
```

- Now onto some more utilities, install the openssh server to allow communication between your host machine and the virtual machine using:

```
sudo apt install openssh-server
```

Don't forget enabling password authentication using:

```
sudo nano /etc/ssh/sshd_config
```

And uncommenting the line PasswordAuthentication yes. Then restart the service with:

```
sudo service ssh restart
```

- Next we want to install FTP protocal to transfer files to the virtual machine. Use:

```
sudo apt install vsftpd
```

We need to enable write service for this service. Edit the config file using:

```
sudo nano /etc/vsftpd.conf
```

And uncomment the line write_enable=YES. Restart the service using:

```
sudo service vsftpd restart
```

Whew. That was a lot of stuff. Let's try accessing our virtual machine from our host machine.


### 5. Acessing our virtual machine using ssh

In your local machine, open a command line and use:

```
ssh [user name on the virtual machine]@[virtual machine ip address]
Example:
ssh tomas@192.168.56.5
```

You'll need to enter your password, but should now find yourself connected to your virtual machine.

- Now let's use an FTP application to transfer files to/from your virtual machine. In this tutorial we will be using FileZilla, you can download it [here](https://filezilla-project.org/)

- Once installed, connect it to our virtual machine, using it's address, username and password. Now you can simultaneously see files on your host machine on the left and virtual machine on the right. 

- Last but not least, let's install Git and Java in our machine.

```
sudo apt install git
sudo apt install openjdk-8-jdk-headless
```

That's it right?


### 6. Let's get down to business

Now we want to clone your repository into our virtual machine. You can do this using:

```
git clone [your repository clone link here]
```

- Let's not forget installing some dependencies:

```
sudo apt install maven
```

- Changing into your ca1 basic tutorial folder, you can run the application using:

```
mvn spring-boot:run
```

You can access the generated web page by opening a browser and typing *[your virtual machine ip address]:[port]*. In my case this was 192.168.56.5:8080.

- Now onto the the chat server from ca2, change into it's directory and use:

```
./gradlew runServer
```

If you get an error, don't forget to add execute permissions to gradlew with *chmod +x gradlew*

- You can run the chat client in your host machine to make sure everything is working. 

*Since this is a desktop application, you cannot run the chat clients in your virtual machine, as it has no GUI.*

To do this, don't forget to edit your runClient task on the build.gradle file. Change the localhost argument to the ip address of your virtual machine.

There! We did it! We made it. Onto part 2......


