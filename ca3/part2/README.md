# Class Assignment 3 Report

Bonjour, it's Vagrrant time. This time we want to setup a virtual machine to execute the tutorial spring boot application developed back in CA2-part2.

## PART2
=================


### -1. Preconditions

You need to have VirtualBox and Vagrant installed in your machine. You should have VirtualBox installed from the previous tutorial. You can download Vagrant [here](https://www.vagrantup.com/downloads).


### 0. Setup

- Let's start by accessing this [link](https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/src/master/) and download the Vagrantfile into your own ca3-part2 folder. This file contains the configuration to setup two virtual machines ("db" and "web"). 

Amazing how all those steps we did back in ca3-part1 can now be done in a couple of minutes.

- Execute the following command to create the two virtual machines:

```
vagrant up
```

And... Magic. After a few minutes, both machines should be up and running the basic spring boot tutorial. You can open the spring web application using one of:

```
http://localhost:8080/basic-0.0.1-SNAPSHOT/
http://192.168.33.10:8080/basic-0.0.1-SNAPSHOT/
```

And you can also access the H2 database using one of:

```
http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console
http://192.168.33.10:8080/basic-0.0.1-SNAPSHOT/h2-console
```

*Remember using "jdbc:h2:tcp://192.168.33.11:9092/./jpadb" for your connection string when accessing the database*

But you know what would be really nice? Acessing the application we made changes to when in ca2-part2.


### 1. Setting up your own spring boot application


It's time we take a look at the Vagrantfile we copied into our repository. We can see there are some general provisions for both virtual machines and then the file goes into detail on the "db" and "web" virtual machine setup. Right now we wanto to look at the "web" VM configurations, as we want to get our own application running here.

Notice there is a *git clone* command near the end. We want the VM to clone our own repository containing the application we want to run.

- Replace these line using your *git clone* link and access the directory containing your application. It should look like this:

```
git clone https://tomas_osswald@bitbucket.org/tomas_osswald/devops-20-21-1201784.git
cd devops-20-21-1201784/ca2/part2/
```


#### Follow this [link](https://bitbucket.org/atb/tut-basic-gradle/src/master/) provided in the class assignment for a series of changes to the application we need to make.


- Notice that your application builds a jar file called "demo-0.0.1-SNAPSHOT.jar". Either change your application to build a jar file matching the one used in Vagrantfile in the settings.gradle file, or edit your Vagrantfile to match the name above. You'll also need to add *id "war"* to your build.gradle file so it can create the war file needed in your Vagrantfile.

- You might also notice that this application will use Tomcat. We should add a dependency in our build.gradle file like this:

```
providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
```

We will need a ServletInitializer class in our application:

```
public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder springApplicationBuilder) {
        return springApplicationBuilder.sources(ReactAndSpringDataRestApplication.class);
    }
}

```

- Add the application context path with this line in your app.js file:

```
client({method: 'GET', path: '/basic-0.0.1-SNAPSHOT/api/employees'}).done(response => {
```

- We need to fix the ref to css in index.html. You need to remove a backslash before main.css:

```
<link rel="stylesheet" href="main.css" />
```

- Lastly, your application.properties file should look like this:

```
server.servlet.context-path=/basic-0.0.1-SNAPSHOT
spring.data.rest.base-path=/api
spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
spring.jpa.hibernate.ddl-auto=update
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
spring.h2.console.settings.web-allow-others=true
```


### 2. Let's run the virtual machines

Now these were a lot of changes. Let's try running our virtual machines to see if everything is running. Use *vagrant up*. You pushed all these changes to your repository, right?


### 3. Of course I did

Now let's try *vagrant up* again.

Oh no! Something went wrong.

We keep getting this error on our build, it says the *assembleFrontend* task can't finish and stops our process. This is weird, because in our host machine the application builds without a problem. It's most likely related to a windows vs linux difference.

We did a lot of double-checking and tried giving permissions to different files and folders in our virtual machine in hope something would work, but nothing changed.


However, we do have in our group of young software development prodigies an illustrious Man of enormous troubleshooting capabilities. Were I a teacher of this phenomenal course, I would not hesitate to give Bruno Tamames [1201762] the best grade and a letter of recomendation.


- In our virtual machine, if we delete the *node* and *node_modules* folders, the application will build correctly.

- Now there are many ways we can solve this. Here we go for a more direct approach. In our Vagrantfile we can add the following lines after changing into the source directory.

```
rm -r node node_modules
```


And that is it! This concludes part2 of class assignment 3! You made it!

You can check those links from step 0 in your browser, everything should be working.