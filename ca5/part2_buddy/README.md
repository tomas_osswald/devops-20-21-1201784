# Class Assignment 5 Report

Continuous Integration / Continuous Deployment Pipelines with Jenkins, now choosing an alternative to Jenkins.

## PART2 - Buddy
=================

### 1. What is Buddy

Buddy is a tool that boasts having the easiest way to introduce Continuous Integration/Continuous Deployment to the delivery process.

Buddy provides:

- Automatic deployments on push to branch
- Docker-based builds and tests
- 10-minute setup of complete environment


### 2. Comparison to Jenkins

While Jenkins requires and installation, Buddy does not. Jenkins comes packaged as a binary and you will need to deploy it, there is no deployment for Buddy.

Regarding maintenance, with Jenkins you are expected to update and might need some tuning if you are unexperienced. Buddy is a managed offering, meaning you don't have to actively maintain it.

Buddy does not have the vast amount of plugins Jenkins has, but it's integrations are pre-made and tested. There are no issues where plugins might interfere with one another.

Much like Jenkins, in Buddy you have two options when creating a pipeline. You can have the UI generate a buddy.yaml file or you can write that file yourself.

Both tools have similar presentation regarding build status.



### 3. How can we achieve the same results


### 4. Implementation

#### 4.1 Setup

Get started by going to [https://buddy.works/docs/integrations](https://buddy.works/docs/integrations) and click "Try Buddy for free". Connect to your bitbucket account and select you devops repository.

Now we were having some trouble with this part as Buddy was not connecting to our Bitbucket repository. This was fixing by cloning our repository to Github and using that one instead.


#### 4.2 Pipeline creation

Once your project is synchronised, select the pipelines tab and click "Add a new pipeline". After choosing a name we can select what action triggers the pipeline. For now we will select "Manual" triggering.

As you can see, we can quickly add a few actions and decide their sequence. Let's mirror our Jenkinsfile and add Assemble, Test, Javadoc, Archive and Publish Docker Image actions.

- For Assembling and testing, we add simple Gradle actions with the code "./gradlew assemble" and "./gradlew test". Remember to add permissions to gradlew with "chmod +x gradlew".

- For Javadoc, we can create a gradle action with the line "./gradlew javadoc". This will generate a javadoc folder in the build/docs folder with all javadoc for this project. However, we haven't found a tool to publish that javadoc yet.

- For Archiving, let's create a zip action and set the path to the war file in build/libs.

- For Docker Image, let's create a "Build Docker image" action, where we specify the Dockerfile to use. Next, add a "Push Docker image" action, where we insert our Docker Hub credentials, as well as the repository we want to push to and a tag.


As you can see, the steps for creating a functioning pipeline were fairly easy and less mistake prone that with jenkins. However, we could not find a way to publish the javadocs online. So it does lack on some features in comparison.


#### 4.3 Solving the javadoc problem

First we tried by hosting the javadocs at a repository on bitbucket (if you name a repository [workspaceID].bitbucket.io it can be accessed directly by typing the name in your browser). However, Buddy does not let us choose a particular folder to push to bitbucket, so the whole project had to be pushed and not just the javadocs folder.

Next, we manage to build a site with the javadoc files using the Jekyll integration in Buddy and adding a line before the build to change the working directory to build/docs/javadoc. However we still need to deploy this site somewhere, as it is now in the Buddy filesystem.




https://stackshare.io/stackups/buddy-vs-jenkins
https://buddy.works/pricing
https://hackernoon.com/buddy-vs-jenkins-mt4v32u2
