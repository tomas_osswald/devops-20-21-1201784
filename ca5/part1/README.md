# Class Assignment 5 Report

Continuous Integration / Continuous Deployment Pipelines with Jenkins with the Gradle Basic Demo project from Class Assignment 2, part 1.

## PART1
=================

### 0. Setup

Let's get started! We need to download Jenkins and run it in our machine. Head on over to the [Jenkins website](https://www.jenkins.io/download/) and download the Generic Java package (.war). Once that is done, open a command line and use:

```
java -jar jenkins.war
```

This should start jenkins on your localhost:8080 port, which you can access in your browser.

You will first be requested a code that you can find in the installation logs. Then, you can choose the initial plugins for your jenkins. Choose the standard plugins option. After that you will be prompted to create an admin account. Lastly, you need to specify the address of your jenkins, which you can leave as the default localhost:8080.

That's it, Jenkins is up and running.


### 1. Pipeline creation

Create a new job of type pipeline. In the pipeline script section, enter the following code:

```
pipeline {
    agent any
    
    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out ...'
                git 'https://bitbucket.org/luisnogueira/gradle_basic_demo'
            }
        }
        stage('Build') {
            steps {
                echo 'Building...'
                sh './gradlew clean build'
            }
        }
        stage ('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'build/distributions/*'
            }
        }
    }
}
```

Next, click save and then "Build now" to build your project. Congratulations, you just built your first pipeline in Jenkins!


### 2. Can I go now?

No.


### 3. Let's apply this pipeline thing to our own project

Again, let's create a new pipeline job in jenkins. Select the option "Pipeline script from SCM". This will make the pipeline run from our own repository, which you will insert in the next field after selecting "Git". 

If your repository is private, you will need to add an entry to your credentials manager so it can use that repository.

Remember to set the script path to the directory of your project where you will also place the following Jenkinsfile:

```
pipeline {
    agent any
    
    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out ...'
                git 'https://bitbucket.org/tomas_osswald/devops-20-21-1201784/src/master/ca2/part1/'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assembling...'
		dir('ca2/part1/') {
                    sh './gradlew assemble'
		}
            }
        }
	stage('Test') {
	    steps {
		echo 'Testing...'
		dir('ca2/part1/') {
		    sh './gradlew test'
		}
	    }
	}
        stage ('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'ca2/part1/build/libs/*'
            }
        }
    }
}
```

Notice that we have separated the build task into an assemble and a test class. Before running the script, we make sure to specify where the gradlew file is located. Lastly we select the build/libs directory in our project directory to store our archives.

That's it! We're done with part1!