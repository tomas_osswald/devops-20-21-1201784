# Class Assignment 5 Report

Continuous Integration / Continuous Deployment Pipelines with Jenkins, now with the Class Assignment 2, Part 2 project.

## PART2
=================

### 0. Setup

Now it's time to setup a pipeline for the project we worked on in CA2, part2. This is a very similar project to the one we worked with in part 1 of this assignment, so to save some time, when creating a new pipeline in Jenkins, use the option to copy the pipeline from an existing one and type in the pipeline from last time.

Now we need to make a few changes. 

- First, in the pipeline configurations, change all paths pointing to ca2/part1 to ca2/part2.

- Then copy the Jenkinsfile located in your ca2/part1 directory to the ca2/part2 directory and edit the fields to point to the correct directory as well.

Don't forget to push these changes to the remote repository and try building the pipeline in Jenkins.

Now in this case we happened to still have the node_modules folder in our remote repository. To fix this, we made sure it was already added in the git_ignore file and ran the command "git rm -r --cached node_modules" to make sure to delete any reference to the cached directory.

After doing this, our pipeline is up and running.

But it's never this simple. We need to fix our Jenkinsfile, because we wan't to have a Javadoc stage that will generate the javadoc of the project and publish it in Jenkins, as well as a Publish Image stage that will generate a docker image with Tomcat and the war file and publish it to Docker Hub.


### 1. Javadoc stage

For the Javadoc stage let's install the Jenkins "Html Publisher" and "Javadoc" plugins. These plugins can be found and installed in the Jenkins settings menu. If you have some updates available, you might as well do them and restart Jenkins.

Then, to generate and publish the javadocs, add the following stage to your Jenkinsfile:

```
	    stage('Javadoc') {
	        steps {
	            dir('ca2/part2/') {
	                sh './gradlew javadoc'
	            }
                javadoc javadocDir: 'ca2/part2/build/docs/javadoc', keepAll: true
	        }
	    }
```

With this, you should see a new button "Javadoc" in your Pipeline page with javadoc information available.

However we want to try to archive and publish these html reports. For this we'll use publishHTML:

```
	    stage('Javadoc') {
	        steps {
		    dir('ca2/part2/') {
	                sh './gradlew javadoc'
	            }
                javadoc javadocDir: 'ca2/part2/build/docs/javadoc', keepAll: true
	            publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'ca2/part2/build/docs/javadoc', reportFiles: 'index.html', reportName: 'Javadoc HTML Report', reportTitles: 'Javadoc'])
	        }
	    }
```

You should find a new button available in your Jenkins pipeline, "Javadoc HTML Report" (or whatever you named it in publishHTML). Clicking here should display your javadoc in html format.

Congrats!


### 2. Docker Image stage

First of all, we need to create a new repository in our docker hub account to push our images to. We created devops_ca5_part2.

Next, download and install the "Docker Pipeline" plugin for Jenkins. Once you have done that, you need to edit your Jenkinsfile to contain the following code:


```
stage("Publish Docker Image") {
            steps{
                echo 'Publishing Image...'
                dir('ca2/part2/'){
                    script{
                        docker.withRegistry('', 'dockerHub') {
                            def dockerImage = docker.build("tomasmco/devops_ca5_part2:${BUILD_NUMBER}")
                            dockerImage.push()
                        }
                    }
                }
            }
        }
```

- notice the "dockerHub" variable, which points to our credentials for Docker Hub which we added to Jenkins. Notice also the parametres for the build method, where we specify our account name and repository.

You will also need to create a Dockerfile with the following code:


```
FROM tomcat

RUN apt-get update -y

RUN apt-get install -f

RUN apt-get install git -y

RUN apt-get install nodejs -y

RUN apt-get install npm -y

RUN mkdir -p /tmp/build

ADD /build/libs/tut-basic-gradle-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8080
```

Try running your pipeline now!

It works! It really does! Holy S***!
