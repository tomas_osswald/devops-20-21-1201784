# Class Assignment 1 Report

Here you should write the report for the assignment.

You should use Markdown Syntax so that the report is properly rendered in Bitbucket. See how to write README files for Bitbucket in [here](https://confluence.atlassian.com/bitbucket/readme-content-221449772.html)

The source code for this assignment is located in the folder [ca1/tut-basic](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca1/tut-basic)

As a suggestion, the report should have the following sections.

## 1. Analysis, Design and Implementation

*Description of the analysis, design and implementation of the requirements*

- Simple changes to the initial project are required in the form of an additional email field in employee data.
- This new field has to be tested and validated, initially regarding null and empty values and lastly regarding email address rules.
- The whole process will take several steps that should be recorded using a Version Control System, in this case Git.
    - New versions of the working application in the master branch should be tagged.
    - A branch should be created to work on new functionalities for the application.
    - When new functionalities are tested and working, they should be merged to the master branch with a new tag.


##Tutorial

### 1.
> You should have a master branch that you use to "publish" the "stable" versions of the Tutorial React.js and Spring Data REST Application.

A master branch already exists by default when a git repository is initialized.

### 2.
> You should have tags in this branch to mark the versions of the application. You should use a pattern like: major.minor.revision (e.g., 1.2.0). Tag the initial version as v1.2.0.

Making sure you are working in the master branch, create a tag using:

```
git tag v1.2.0
```

and push the tag into the remote branch:

```
git push origin v1.2.0
```

### 3.
> You should develop new features in branches named after the feature. For instance, a branch named "email-field" to add a new email field to the application. You should create a branch called email-field.

To create a branch in git use:

```
git branch email-Field
```

to create a branch and immediately change to it:

```
git checkout -b email-Field
```

> You should add support for email field.

Add an attribute email

> You should also add unit tests for testing the creation of Employees and the validation of their attributes (for instance, no null/empty values).

> You should debug the server and client parts of the solution.

> When the new feature is completed (and tested) the code should be merged with the master and a new tag should be created (e.g, v1.3.0).

### 4.
> You should also create branches for fixing bugs (e.g., fix-invalid-email")

> Create a branch called fix-invalid-email. The server should only accept Employees with a valid email (e.g., an email must have the "@" sign).

> You should debug the server and client parts of the solution.

> When the fix is completed (and tested) the code should be merged into master and a new tag should be created (with a change in the minor number, e.g., v1.3.0 -> v1.3.1)

### 5.
> At the end of the assignment mark your repository with the tag ca1.



* *Should include a description of the steps used to achieve the requirements*
* *Should include justifications for the options (when required)*

## 2. Analysis of an Alternative

*For this assignment is there an aterntiave tool for Git? Describe it and compare it to Git*

## 3. Implementation of the Alternative

*Present the implementation of this class assignment using the git alternative*