# Class Assignment 1 Report

Here you should write the report for the assignment.

You should use Markdown Syntax so that the report is properly rendered in Bitbucket. See how to write README files for Bitbucket in [here](https://confluence.atlassian.com/bitbucket/readme-content-221449772.html)

The source code for this assignment is located in the folder [ca1/tut-basic](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca1/tut-basic)

As a suggestion, the report should have the following sections.

## 1. Analysis, Design and Implementation

*Description of the analysis, design and implementation of the requirements*

- Simple changes to the initial project are required in the form of an additional email field in employee data.
- This new field has to be tested and validated, initially regarding null and empty values and lastly regarding email address rules.
- The whole process will take several steps that should be recorded using a Version Control System, in this case Git.
    - New versions of the working application in the master branch should be tagged.
    - A branch should be created to work on new functionalities for the application.
    - When new functionalities are tested and working, they should be merged to the master branch with a new tag.


##Tutorial

### 1.
> You should have a master branch that you use to "publish" the "stable" versions of the Tutorial React.js and Spring Data REST Application.

A master branch already exists by default when a git repository is initialized.

---
### 2.
> You should have tags in this branch to mark the versions of the application. You should use a pattern like: major.minor.revision (e.g., 1.2.0). Tag the initial version as v1.2.0.

Making sure you are working in the master branch, create a tag using:

```
git tag v1.2.0
```

and push the tag into the remote branch:

```
git push origin v1.2.0
```
---
### 3.
> You should develop new features in branches named after the feature. For instance, a branch named "email-field" to add a new email field to the application. You should create a branch called email-field.

Create a branch in git with:

```
git branch email-Field
```

Change the working branch to the new one using:

```
git checkout email-Field
```

Create a branch and immediately change to it using:

```
git checkout -b email-Field
```

You can push the newly created branch to the remote repository using:

```
git push origin email-Field
```

---
> You should add support for email field.

Add an attribute email in the Employee.java. Getters and Setters should also be created.

Add the necessary fields in the app.js file to display the table with updated columns.

---
> You should also add unit tests for testing the creation of Employees and the validation of their attributes (for instance, no null/empty values).

Add methods for all attributes in the Employee.java to validate that they are not null, empty or blank.

Add unit tests to EmployeeTest.java.

---
> You should debug the server and client parts of the solution.

Run the app and verify that the email field has been implemented correctly

---
> When the new feature is completed (and tested) the code should be merged with the master and a new tag should be created (e.g, v1.3.0).

Checkout back into the master branch using:

```
git checkout master
```

Merge the email-Field branch into the master using:

```
git merge email-Field
```

Solve any issues using your preferred text editor. Once the merge is complete, push changes into the remote repository. Create and push a new tag using:

```
git tag v1.3.0
git push origin v1.3.0
```

---
### 4.
> You should also create branches for fixing bugs (e.g., fix-invalid-email"). Create a branch called fix-invalid-email.

Create a new branch using:

```
git checkout -b fix-invalid-email
```

---
> The server should only accept Employees with a valid email (e.g., an email must have the "@" sign). You should debug the server and client parts of the solution.

Change the email field validation to check for an "@" sign.

Run the app and verify that the new email field validation has been implemented correctly.

---
> When the fix is completed (and tested) the code should be merged into master and a new tag should be created (with a change in the minor number, e.g., v1.3.0 -> v1.3.1)

Checkout back into the master branch using:

```
git checkout master
```

Merge the email-Field branch into the master using:

```
git merge email-Field
```

Solve any issues using your preferred text editor. Once the merge is complete, push changes into the remote repository. Create and push a new tag using:

```
git tag v1.3.1
git push origin v1.3.1
```

---
### 5.
> At the end of the assignment mark your repository with the tag ca1.

Create a new tag for the repository using:


```
git tag ca1
git push origin ca1
```

---

## 2. Analysis of an Alternative

*For this assignment is there an aterntiave tool for Git? Describe it and compare it to Git*
There are many alternative to Git. For this assignment, Plastic SCM was chosen.

## Plastic SCM

Plastic SCM is a version control system with the ability to handle large files and repositories with ease, which makes it ideal to projects such as game development.

The system uses a graphic user interface, allowing visualization of repositories, branches and tasks. A special GUI is included for users who may not be familiar with coding, such as artists.

Cloud hosting is made through the Plastic SCM's own Plastic Cloud. A free cloud organization provides 5gb storage, but there are paid options that provide more storage.

It provides both a centralized and distributed

# Workflow
Everything to be done in code must be a "Task", regardless of longevity. These tasks can be handled by issue trackers like Jira. For every task a "Task Branch" will be created. This Task should be reviewed by people on the team and finally merged back into to the main branch.

Every branch starts empty and a "checkin" will be made for initial changes. Frequent and smaller checkins are recommended so you can follow the branches' progress.

When the task is solved, the branch will merge with the main branch. For this merge, a temporary checkin is created in the main branch and tests will be executed to make sure everything is working. The checkin will then be confirmed and the process is over.

## 3. Implementation of the Alternative

The following tutorial will explain the step by step process for the assigned task using Plastic SCM GUI. We will skip the changes to the application code.

### 1.
> You should have a master branch that you use to "publish" the "stable" versions of the Tutorial React.js and Spring Data REST Application.

A master branch already exists by default when a Plastic SCM repository is initialized.

---
### 2.
> You should have tags in this branch to mark the versions of the application. You should use a pattern like: major.minor.revision (e.g., 1.2.0). Tag the initial version as v1.2.0.

Create a tag go to the "Label" tab and select "Create a new label...". By default Plastic will give the new label to the current changeset. However a different one can be chosen. Insert v1.2.0 as the label's name and click Ok.

---
### 3.

> You should develop new features in branches named after the feature. For instance, a branch named "email-field" to add a new email field to the application. You should create a branch called email-field.

Create a branch in git by cliking on the branch explorer tab. Once there, select the desired changeset you wish to create a branch from, right click and select "Create branch from this changeset..."

A branch name of your choice can be inserted in the "Branch name:" field. Use "email-field".

To change to this new branch, check the "Switch workspace to this branch" box. Otherwise select the new Branch once created in the branch explorer tab, right click and choose "Switch workspace to this branch".

You don't need to push your changes to the cloud.

---
> When the new feature is completed (and tested) the code should be merged with the master and a new tag should be created (e.g, v1.3.0).

In the branch explorer tab, right click the branch you wish to merge and select "Merge from this branch...". In the following menu you can add some details such as comments and authors. Once done, select Process all merges which will conclude the merging process.

Select the Pending changes tab to view the new merge. Any issues will show up and can be solved if you click to show differences. Once done, you can click the Checkin and the merge is done.

Finally, to create a tag go to the "Label" tab and select "Create a new label...". Insert v1.3.0 as the label's name and click Ok.

---
### 4.
> You should also create branches for fixing bugs (e.g., fix-invalid-email"). Create a branch called fix-invalid-email.

Once again in the branch explorer tab, right click the current changeset and select "Create branch from this changeset...". Choose the name "fix-invalid-email".

---
> When the fix is completed (and tested) the code should be merged into master and a new tag should be created (with a change in the minor number, e.g., v1.3.0 -> v1.3.1)

Again, in the branch explorer tab, right click the branch you wish to merge and select "Merge from this branch...". Once done, select Process all merges which will conclude the merging process.

Select the Pending changes tab to view the new merge. Any issues will show up and can be solved if you click to show differences. Once done, you can click the Checkin and the merge is done.

Finally, to create a tag go to the "Label" tab and select "Create a new label...". Insert v1.3.1 as the label's name and click Ok.

---
### 5.
> At the end of the assignment mark your repository with the tag ca1.

Create a new label for the repository using the same methods as above. Using the branch explorer tab, you can right click different changesets and choose a label from your label list.
