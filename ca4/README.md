# Class Assignment 4 Report

Welcome to the Containers with Docker tutorial, let's get started.

### 0. Setup

We'll need to install Docker for this class assignment. Go ahead and download and install Docker for Desktop from this [link](https://www.docker.com/products/docker-desktop).

Once that is done, let's download the following template repository from this [link](https://bitbucket.org/atb/docker-compose-spring-tut-demo/src/master/).


### 1. Building docker images

Now you can examine the Dockerfiles and you'll notice that they are not using your repository (obviously). Let's fix that, open the Dockerfile in the web folder and change the link to your repository's clone link:

```
RUN git clone https://tomas_osswald@bitbucket.org/tomas_osswald/devops-20-21-1201784.git

WORKDIR /tmp/build/devops-20-21-1201784/ca2/part2
```

*Make sure your gradlew file has execute permissions, consider using RUN chmod u+x gradlew if you get an error.*

In the same folder as the docker-compose.yml file, you can run the following command to build the containers:

```
docker-compose build
```

Once that is done, you can execute the containers using:

```
docker-compose up
```

You now have containers running web and db. To access the web application or the database you can use the following links in your browser:

- WEB:
[http://localhost:8080/basic-0.0.1-SNAPSHOT/](http://localhost:8080/basic-0.0.1-SNAPSHOT/)

- DB:
[http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console](http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console)

Use the connection string jdbc:h2:tcp://192.168.33.11:9092/./jpadb for h2 access.

You can also access your containers using:

```
docker-compose exec [container name] bash
```


### 2. Publish to Docker Hub

First we need to create a Docker account if you haven't already. Next, head on over to [Docker Hub](https://hub.docker.com) and sign in.

Let's create a new repository under the "repositories" tab so we can upload our images.

Notice that you can connect your repository to Github or Bitbucket so it will run tests and build every time you push something. For now this is not needed.

Back to our command line, let's tag the images we created to our repository using

```
docker tag [image name] [docker hub user name]/[repository name]
docker tag ca4_web tomasmco/devops_ca4
```

After tagging both images, we can push these changes to the hub using:

```
docker push [docker hub user name]/[repository name]
docker push tomasmco/devops_ca4
```


### 3. Use volume to get copy of database file

If you inspect your docker-compose.yml you will notice that the "db" container has a volume declared:

```
    volumes:
      - ./data:/usr/src/data
```

This means that whatever file is in a data directory in your docker-compose.yml directory will show up in the /usr/src/date directory in the db container. So to get a copy of the database file in the container we will need to start the container up and log into it.

After running the containers, you can log into one using:

```
docker exec [container name] bash
```

Navigate into the /usr/src/app directory where the jpadb.mv.db file is located. Copy that file using:

```
cp jpadb.mv.db /usr/src/data
```

The database file should now show up in the data directory in your physical machine as well as in the container. Congratulations, you did it!


### 4. Heroku - alternative implementation

Well it's never that easy, is it? Let's explore an alternative to Docker, Heroku. Heroku is a platform as a service (PaaS) that enables developers to build, run, and operate applications entirely in the cloud.

That all seems fine and dandy, but how do we do it? Let's find out.


### 5. Setup

Register for an account on [Heroku](https://www.heroku.com/). Once that is done, create a new app in Heroku. You need to specify a name for your application that will be it's URL. In this scenario we used "devops-ca-4".

Now open your app in the browser and select the "deploy" tab. You will get some instructions on how to deploy your build to heroku. First, download and install the [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli#download-and-install). Then, to link your local repository to the Heroku repository use:

```
heroku git:remote -a [app name]
Ex.:
heroku git:remote -a devops-ca-4
```

### 6. Deploying the code to heroku

We want to deploy only a folder from our devops repository to Heroku. For this purpose you can use:

```
git subtree push --prefix [subtree-folder] heroku master
Ex.:
git subtree push --prefix ca2/part2 heroku master
```

Running "git remote" should now show you that your local repository is connected to a "heroku" repository. However your build should fail


### 7. Retreat! Regroup!

